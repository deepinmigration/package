# Live 软件包定制

### TODO 
* 目前`source`只判断了'git'类型下载代码及生成代码和版本号识别
* 版本号生成规则修正
* 生成软件包需要导入编译环境

### 定制软件
裁剪功能, 删除依赖等

#### 准备源码
编辑 `packages.yaml`
```
source-tool --download
source-tool --export
```

#### 生成编译环境
```
cowbuilder-tool --prepare --instance {INSTANCE}
cowbuilder-tool --create --instance {INSTANCE}
```

#### 编译软件包
```
source-tool --build
```

#### 导出编译软件包
```
source-tool --save
```
默认保存到`chroot.packages`下

然后可将生成好的软件与iso生成工具进行live系统的生成

### 软件定制方式
#### packages.yaml 文件
`packages.yaml`记录软件列表及对应信息


#### packages 文件夹
* {pkgname}/overrides: 覆盖源码文件
* {pkgname}/patches: 源码补丁
* {pkgname}/scripts: 在源码目录执行脚本
