#!/usr/bin/env python3

import subprocess
import os
import yaml
import glob

kSourcesCache = os.path.abspath('source/source-cache')
kBuildCache = os.path.abspath('source/build-cache')
kSourcesResult = os.path.abspath('source/source-result')
kBuildsResult = os.path.abspath('source/source-result')
kPackageYaml = 'packages.yaml'

class Package():
    def __init__(self, kwargs):
        self.name = kwargs['name']
        self.source = kwargs['source']
        self.ref = kwargs.get('ref', 'origin/HEAD') 
        self.version = None
        self.exports = kwargs.get('exports', [])

    def download_source(self):
        if self.source.startswith('git::'):
            if not os.path.exists(os.path.join(kSourcesCache, self.name + '.git')):
                subprocess.check_call(['git', 'clone', '--bare', self.source[5:], self.name + '.git'], cwd=kSourcesCache)
            else:
                subprocess.check_call(['git', 'fetch', '--all', '-p'], cwd=os.path.join(kSourcesCache, self.name + '.git'))
        elif self.source.startswith('dsc::'):
            subprocess.check_call(['dget', '-ud', self.source[5:]], cwd=kSourcesCache)
        else:
            if '::' in self.source:
                dest, url = self.source.split('::')
            else:
                dest = self.source.split('/')[-1]
                url = self.source
            proc_cmd = '/usr/bin/curl -fLC - -retry 3 --retry-delay -3 -o {} {}'.format(dest, url).split() 
            subprocess.check_call(proc_cmd, cwd=kSourcesCache)

    def export_source(self):
        if self.source.startswith('git::'):
            source_dir = os.path.join(kBuildCache, self.name)
            print(' > Export git source')
            if os.path.exists(source_dir):
                return
                subprocess.check_call(['rm', '-rf', source_dir])
            subprocess.check_call(['git', 'clone', os.path.join(kSourcesCache, self.name + '.git'), self.name], cwd=kBuildCache)
            subprocess.check_call(['git', 'checkout', '--force', '--no-track', '-B', 'makepkg', self.ref], cwd=source_dir)

            version = subprocess.check_output("git describe --long", shell=True, cwd=source_dir)
            version = version.strip().decode('utf-8').split('-')
            self.version = '{}.r{}.{}'.format(version[0], version[1], version[2])
        else:
            source_dir = os.path.join(kBuildCache, self.name)

            if os.path.exists(source_dir):
                subprocess.check_call(['rm', '-rf', source_dir])
            if '::' in self.source:
                dest = self.source.split('::')[0]
            else:
                dest = self.source.split('/')[-1]

            subprocess.check_call(['tar', 'xf', os.path.join(kSourcesCache, dest)], cwd=kBuildCache)

        print('I: Get version %s' % self.version)
        # Apply modify build config or patches
        if os.path.exists(os.path.join('packages', self.name, 'patches')):
            for dir, _, patches in os.walk(os.path.join('packages', self.name, 'patches')):
                for patch in patches:
                    print(' > Apply %s' % patch)
                    patch = os.path.abspath(os.path.join(dir, patch))
                    subprocess.check_call(['patch', '-Np1', '-i', patch], cwd=source_dir)

        if os.path.exists(os.path.join('packages', self.name, 'overrides')):
            print(' > Override source')
            overrides_dir = os.path.join('packages', self.name, 'overrides')
            command = 'find . | cpio -dmupu --no-preserve-owner %s' % (source_dir)
            subprocess.check_call(command, shell=True, cwd=overrides_dir)

        if os.path.exists(os.path.join('packages', self.name, 'scripts')):
            for dir, _ , scripts in os.walk(os.path.join('packages', self.name, 'scripts')):
                for script in scripts:
                    print(' > Run %s' % script)
                    script = os.path.abspath(os.path.join(dir, script))
                    subprocess.check_call(script, shell=True, cwd=source_dir)

        # Clean unused files
        if os.path.exists(os.path.join(source_dir, '.git')):
            subprocess.check_call(['rm', '-rf', os.path.join(source_dir, '.git')])

        with open(os.path.join(source_dir, 'debian', 'source', 'format'), 'w') as fp:
            fp.write('3.0 (native)\n')

        command = "DEBEMAIL=livesystem@deepin.com DEBFULLNAME=livesystem dch --force-distribution -b --distribution unreleased  -v " + self.version + ' "Unreleased for livefilesystem."'
        subprocess.check_call(command, shell=True, cwd=source_dir)
        # Clean used dsc file
        subprocess.check_call('rm -f %s_*.dsc' % self.name, shell=True, cwd=kSourcesResult)
        subprocess.check_call('dpkg-source -b %s' % source_dir, shell=True, cwd=kSourcesResult)

    def build_package(self, instance='deepin'):
        if os.path.exists(os.path.join(kBuildsResult, self.name + '.done')):
            print(' > %s has already built' % self.name)
            return

        dsc = glob.glob(os.path.join(kSourcesResult, '%s_*.dsc' % self.name))[0]
        pkgver = os.path.basename(dsc).split('_', 1)[1][:-4]
        command = "sudo ./cowbuilder-tool --build --instance {} --dsc {}".format(instance, dsc)
        subprocess.check_call(command, shell=True)
        subprocess.check_call('touch %s.done' % self.name, shell=True, cwd=kBuildsResult)
    
    def export_packages(self, instance='deepin', dist='unstable', arch='amd64', destdir='chroot.packages'):
        exports = []
        dsc = glob.glob(os.path.join(kSourcesResult, '%s_*.dsc' % self.name))[0]
        pkgver = os.path.basename(dsc).split('_', 1)[1][:-4]
        result = os.path.join('instances', instance, 'results', self.name, pkgver, '%s-%s' % (dist, arch))
        for _, _, files in os.walk(result):
            for file in files:
                if file.endswith('.deb'):
                    for export in self.exports:
                        if file.startswith('%s_%s' % (export, pkgver)):
                            exports.append(os.path.join(result, file))
        if not os.path.exists(destdir):
            subprocess.check_call(['mkdir', '-p', destdir])
        print(" > %s: save exports path:" % self.name)
        for package in exports:
            print("    copying %s" % package)
            subprocess.check_call(['install', '-Dm644', package, os.path.join(destdir, os.path.basename(package))])

if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser()
    for dir in [kBuildCache, kSourcesCache, kSourcesResult, kBuildsResult]:
        if not os.path.exists(dir):
            subprocess.check_call(['mkdir', '-p', dir])

    yaml_dict = yaml.load(open(kPackageYaml))
    parser.add_argument("--download", action="store_true")
    parser.add_argument("--export", action="store_true")
    parser.add_argument("--build", action="store_true")
    parser.add_argument("--save", action="store_true")
    args = parser.parse_args()

    if args.download:
        for pkg in yaml_dict['packages']:
            p = Package(pkg)
            p.download_source()

    if args.export:
        for pkg in yaml_dict['packages']:
            p = Package(pkg)
            p.export_source()

    if args.build:
        for pkg in yaml_dict['packages']:
            p = Package(pkg)
            p.build_package()
    if args.save:
        for pkg in yaml_dict['packages']:
            p = Package(pkg)
            p.export_packages()
